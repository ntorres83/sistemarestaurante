﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using SistemaRestaurante.BusinessEntity;
using SistemaRestaurante.Utils;

namespace SistemaRestaurante.DataAcess
{
    public class PedidoDAL : Singleton<PedidoDAL>
    {
        private readonly Database BaseDatos = DatabaseFactory.CreateDatabase();

        public IList<Pedido> Listar()
        {
            var pedidos = new List<Pedido>();
            var comando = BaseDatos.GetStoredProcCommand("usp_SelectAll_Pedido");

            using (var lector = BaseDatos.ExecuteReader(comando))
            {
                while (lector.Read())
                {
                    pedidos.Add(new Pedido
                    {
                        IdPedido = lector.GetInt32(lector.GetOrdinal("IdPedido")),
                        IdCliente = lector.GetInt32(lector.GetOrdinal("IdCliente")),
                        IdLocal = lector.GetInt32(lector.GetOrdinal("IdLocal")),
                        Cliente = lector.GetString(lector.GetOrdinal("Cliente")),
                        Local = lector.GetString(lector.GetOrdinal("Local")),
                        TipoPedido = lector.GetInt32(lector.GetOrdinal("TipoPedido"))
                       
                    });
                }
            }
            comando.Dispose();
            return pedidos;
        }

        public Pedido Obtener(int idPedido)
        {
            var entidad = new Pedido();
            var comando = BaseDatos.GetStoredProcCommand("usp_Select_Pedido");
            BaseDatos.AddInParameter(comando, "IdProducto", DbType.Int32, idPedido);
            using (var lector = BaseDatos.ExecuteReader(comando))
            {
                if (lector.Read())
                {
                    entidad.IdPedido = lector.GetInt32(lector.GetOrdinal("IdPedido"));
                    entidad.IdCliente = lector.GetInt32(lector.GetOrdinal("IdCliente"));
                    entidad.IdLocal = lector.GetInt32(lector.GetOrdinal("IdLocal"));
                    entidad.Cliente = lector.GetString(lector.GetOrdinal("Cliente"));
                    entidad.Local = lector.GetString(lector.GetOrdinal("Local"));
                    entidad.TipoPedido = lector.GetInt32(lector.GetOrdinal("TipoPedido"));
                }
            }
            comando.Dispose();
            return entidad;
        }

        public int Agregar(Pedido entidad)
        {
            using (DbConnection conection = BaseDatos.CreateConnection())
            {
                conection.Open();
                using (DbTransaction transaction = conection.BeginTransaction())
                {
                    try
                    {
                        var comando = BaseDatos.GetStoredProcCommand("usp_Insert_Pedido");
                        BaseDatos.AddOutParameter(comando, "IdPedido", DbType.Int32, 4);
                        BaseDatos.AddInParameter(comando, "IdCliente", DbType.Int32, entidad.IdCliente);
                        BaseDatos.AddInParameter(comando, "IdLocal", DbType.Int32, entidad.IdLocal);
                        BaseDatos.AddInParameter(comando, "Fecha", DbType.DateTime, entidad.Fecha);
                        BaseDatos.AddInParameter(comando, "TipoPedido", DbType.Int32, entidad.TipoPedido);
                       
                        var resultado = BaseDatos.ExecuteNonQuery(comando, transaction);
                        if (resultado == 0) throw new Exception("Error al Agregar Pedido.");
                        
                        var idPedido = (int)BaseDatos.GetParameterValue(comando, "IdPedido");

                        comando = BaseDatos.GetStoredProcCommand("usp_Insert_DetallePedido");
                        BaseDatos.AddInParameter(comando, "IdPedido", DbType.Int32);
                        BaseDatos.AddInParameter(comando, "IdItem", DbType.Int32);
                        BaseDatos.AddInParameter(comando, "IdProducto", DbType.Int32);
                        BaseDatos.AddInParameter(comando, "Cantidad", DbType.Int32);
                        BaseDatos.AddInParameter(comando, "Precio", DbType.Int32);
                        

                        foreach (var detalle in entidad.DetallePedido)
                        {
                            BaseDatos.SetParameterValue(comando, "IdPedido", idPedido);
                            BaseDatos.SetParameterValue(comando, "IdItem", detalle.IdItem);
                            BaseDatos.SetParameterValue(comando, "IdProducto", detalle.IdProducto);
                            BaseDatos.SetParameterValue(comando, "Cantidad", detalle.Cantidad);
                            BaseDatos.SetParameterValue(comando, "Precio", detalle.Precio);

                            resultado = BaseDatos.ExecuteNonQuery(comando, transaction);
                            if (resultado == 0) throw new Exception("Error al Agregar DetalleHojaTrabajoCU.");
                        }

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }

            return 1;
        }

        public bool Modificar(Pedido entidad)
        {
            using (DbConnection conection = BaseDatos.CreateConnection())
            {
                conection.Open();
                using (DbTransaction transaction = conection.BeginTransaction())
                {
                    try
                    {
                        var comando = BaseDatos.GetStoredProcCommand("usp_Update_Pedido");
                        BaseDatos.AddInParameter(comando, "IdPedido", DbType.Int32, entidad.IdPedido);
                        BaseDatos.AddInParameter(comando, "IdCliente", DbType.Int32, entidad.IdCliente);
                        BaseDatos.AddInParameter(comando, "IdLocal", DbType.Int32, entidad.IdLocal);
                        BaseDatos.AddInParameter(comando, "Fecha", DbType.DateTime, entidad.Fecha);
                        BaseDatos.AddInParameter(comando, "TipoPedido", DbType.Int32, entidad.TipoPedido);

                        var resultado = BaseDatos.ExecuteNonQuery(comando, transaction);
                        if (resultado == 0) throw new Exception("Error al Modificar Pedido.");

                        comando = BaseDatos.GetStoredProcCommand("usp_Delete_DetallePedido_ByIdPedido");
                        BaseDatos.AddInParameter(comando, "IdPedido", DbType.Int32, entidad.IdPedido);

                        resultado = BaseDatos.ExecuteNonQuery(comando);
                        if (resultado == 0) throw new Exception("Error al Eliminar Detalles.");

                       comando = BaseDatos.GetStoredProcCommand("usp_Insert_DetallePedido");
                        BaseDatos.AddInParameter(comando, "IdPedido", DbType.Int32);
                        BaseDatos.AddInParameter(comando, "IdItem", DbType.Int32);
                        BaseDatos.AddInParameter(comando, "IdProducto", DbType.Int32);
                        BaseDatos.AddInParameter(comando, "Cantidad", DbType.Int32);
                        BaseDatos.AddInParameter(comando, "Precio", DbType.Int32);
                        

                        foreach (var detalle in entidad.DetallePedido)
                        {
                            BaseDatos.SetParameterValue(comando, "IdPedido", entidad.IdPedido);
                            BaseDatos.SetParameterValue(comando, "IdItem", detalle.IdItem);
                            BaseDatos.SetParameterValue(comando, "IdProducto", detalle.IdProducto);
                            BaseDatos.SetParameterValue(comando, "Cantidad", detalle.Cantidad);
                            BaseDatos.SetParameterValue(comando, "Precio", detalle.Precio);

                            resultado = BaseDatos.ExecuteNonQuery(comando, transaction);
                            if (resultado == 0) throw new Exception("Error al Agregar Detalle.");
                        }


                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                }
            }

            return true;

        }

        public bool Eliminar(int idPedido)
        {
            var comando = BaseDatos.GetStoredProcCommand("usp_Delete_Pedido");
            BaseDatos.AddInParameter(comando, "IdPedido", DbType.Int32, idPedido);

            var resultado = BaseDatos.ExecuteNonQuery(comando);
            if (resultado == 0) throw new Exception("No se pudo eliminar el Pedido");

            comando.Dispose();

            return true;
        }

    }
}
