﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SistemaRestaurante.Utils;

namespace SistemaRestaurante.WebSite.Controllers
{
    using SistemaRestaurante.BusinessLogic;
    using SistemaRestaurante.BusinessEntity;

      [Authorize]
    public class LocalController : Controller
    {
        //
        // GET: /Local/

        public ActionResult Index()
        {
           
            return View("ListadoJqgrid");
        }

       [HttpPost]
        public JsonResult Listar(string sidx, string sord, int page, int rows, bool _search, string searchField, string searchOper, string searchString)
        {
            var jqgrid = new JQgrid();
            IList<Local> lista = new List<Local>();
            var where = string.Empty;

            try
            {
                if (_search)
                {
                    where = where + GetWhere(searchField, searchOper, searchString);
                }

                var totalPages = 0;
                var count = LocalBL.Instancia.Count(where);

                if (count > 0 && rows > 0)
                {
                    totalPages = count / rows;
                    totalPages = totalPages == 0 ? 1 : totalPages;
                }

                page = page > totalPages ? totalPages : page;

                var start = rows * page - rows;
                if (start < 0) start = 0;

                jqgrid.total = totalPages;
                jqgrid.page = page;
                jqgrid.records = count;
                jqgrid.start = start;

                lista = LocalBL.Instancia.Listar(sidx, sord, rows, start, count, where);

                var filas = new List<Row>();

                foreach (var local in lista)
                {
                    filas.Add(new Row
                                  {
                                      id = local.IdLocal,
                                      cell = new string[]
                                                 {
                                                     local.IdLocal.ToString(),
                                                     local.Nombre,
                                                     local.Direccion,
                                                     local.Distrito,
                                                     local.Telefono,
                                                 }
                                  });
                }

                jqgrid.rows = filas.ToArray();
                return Json(jqgrid);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


          public ActionResult Create()
        {
            var local = new Local();

            var provincias = new List<SelectListItem>();
            provincias.Add(new SelectListItem{ Text = "Lima", Value = "Lima"});
            provincias.Add(new SelectListItem { Text = "Trujillo", Value = "Trujillo" });
            provincias.Add(new SelectListItem { Text = "Canta", Value = "Canta" });
            provincias.Add(new SelectListItem { Text = "Huaral", Value = "Huaral" });
            ViewBag.Provincias = new SelectList(provincias, "Value", "Text");
            return View("Create", local);
        }

        public JsonResult Distritos(string id)
        {
            var distritos = new List<SelectListItem>();
            if(id == "Lima")
            {
                distritos.Add(new SelectListItem{ Text = "Jesus Maria", Value = "Jesus Maria"});
                distritos.Add(new SelectListItem { Text = "San Isidro", Value = "San Isidro" });

            }
            else
            {
                if(id=="Trujillo")
                {
                    distritos.Add(new SelectListItem { Text = "Salaverry", Value = "Salaverry" });
                    distritos.Add(new SelectListItem { Text = "Florencia", Value = "Florencia" });

                }
            }
            return Json(new SelectList(distritos, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(Local local)
        {
            LocalBL.Instancia.Agregar(local);

            var lista = LocalBL.Instancia.Listar();
            return View("Listado", lista);
        }

        public ActionResult Edit(int idLocal)
        {
            var local = LocalBL.Instancia.Obtener(idLocal);

            return View("Edit", local);

        }

        [HttpPost]
        public ActionResult Edit(Local local)
        {
            LocalBL.Instancia.Modificar(local);

            var lista= LocalBL.Instancia.Listar();
            return View("Listado", lista);
        }

        public ActionResult Delete(int idLocal)
        {
            var local = LocalBL.Instancia.Obtener(idLocal);

            return View("Delete", local);
        }

        [HttpPost]
        public ActionResult Delete(Local local)
        {
            LocalBL.Instancia.Eliminar(local.IdLocal);

            var lista= LocalBL.Instancia.Listar();
            return View("Listado", lista);
        }

        public ActionResult Details(int idLocal)
        {
            var local = LocalBL.Instancia.Obtener(idLocal);

            return View("Details", local);
        }


        protected String GetWhere(string columna, string operacion, string valor)
        {
            var opciones = new Dictionary<string, string>
                               {
                                   {"eq", "="},
                                   {"ne", "<>"},
                                   {"lt", "<"},
                                   {"le", "<="},
                                   {"gt", ">"},
                                   {"ge", ">="},
                                   {"bw", "LIKE"},
                                   {"bn", "NOT LIKE"},
                                   {"in", "LIKE"},
                                   {"ni", "NOT LIKE"},
                                   {"ew", "LIKE"},
                                   {"en", "NOT LIKE"},
                                   {"cn", "LIKE"},
                                   {"nc", "NOT LIKE"}
                               };

            if (operacion.Equals("bw") || operacion.Equals("bn"))
            {
                valor = valor + "%";
            }
            if (operacion.Equals("ew") || operacion.Equals("en"))
            {
                valor = "%" + valor;
            }
            if (operacion.Equals("cn") || operacion.Equals("nc") || operacion.Equals("in") || operacion.Equals("ni"))
            {
                valor = "%" + valor + "%";
            }
            if (opciones.Take(6).ToDictionary(p => p.Key, p => p.Value).ContainsKey(operacion))
            {
                return string.IsNullOrWhiteSpace(valor)
                           ? string.Empty
                           : columna + " " + opciones[operacion] + " " + valor;
            }
            return columna + " " + opciones[operacion] + " '" + valor + "'";
        }

          [HttpPost]
          public JsonResult CreateJqGrid(Local local)
          {
              var jsonResponse = new JsonResponse();
              try
              {
                  LocalBL.Instancia.Agregar(local);
                  jsonResponse.Success = true;
                  jsonResponse.Message = "Se registro satisfactoriamente";

              }
              catch (Exception ex)
              {
                  jsonResponse.Success = false;
                  jsonResponse.Message = "No se pudo agregar el registro." + ex.Message;
              }
              return Json(jsonResponse, JsonRequestBehavior.AllowGet);
          }

          [HttpPost]
          public JsonResult EditJqGrid(Local local)
          {
              var jsonResponse = new JsonResponse();
              try
              {
                  LocalBL.Instancia.Modificar(local);
                  jsonResponse.Success = true;
                  jsonResponse.Message = "Se modifico satisfactoriamente";

              }
              catch (Exception ex)
              {
                  jsonResponse.Success = false;
                  jsonResponse.Message = "No se pudo modificar el registro." + ex.Message;
              }
              return Json(jsonResponse, JsonRequestBehavior.AllowGet);
          }

          [HttpPost]
          public JsonResult DeleteJqGrid(int id)
          {
              var jsonResponse = new JsonResponse();
              try
              {
                  LocalBL.Instancia.Eliminar(id);
                  jsonResponse.Success = true;
                  jsonResponse.Message = "Se eliminó satisfactoriamente";

              }
              catch (Exception ex)
              {
                  jsonResponse.Success = false;
                  jsonResponse.Message = "No se pudo eliminar el registro." + ex.Message;
              }
              return Json(jsonResponse, JsonRequestBehavior.AllowGet);
          }

    }
}
