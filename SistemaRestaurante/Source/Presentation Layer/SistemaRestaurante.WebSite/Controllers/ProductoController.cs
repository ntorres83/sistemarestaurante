﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistemaRestaurante.WebSite.Controllers
{
    using SistemaRestaurante.BusinessEntity;
    using SistemaRestaurante.BusinessLogic;

    [Authorize]
    public class ProductoController : Controller
    {
        //
        // GET: /Producto/

        public ActionResult Index()
        {
            var lista = ProductoBL.Instancia.Listar();
            return View("Listado", lista);
        }

        public PartialViewResult TestIndex()
        {
            return PartialView("Listado");
        }

        public ActionResult RedirectResultTest()
        {
            return Redirect("http://www.google.com.pe");
        }

        public RedirectResult RedirectResultTest2()
        {
            return Redirect("http://www.google.com.pe");
        }

        public RedirectToRouteResult RedirectRouteTest()
        {
            return RedirectToRoute("Prueba");
        }

        public ContentResult ContentResultTest()
        {
            return Content("Test de Content Result");
        }

        public  JsonResult JsonResultTest()
        {
            var producto = new Producto {Nombre = "Pollo Frito", Precio = 10, IdCategoria = 2};
            return Json(producto);
        }

        public FileResult  FileResultTest()
        {
            return File("/Web.config", "text/html");
        }

        public ActionResult Create()
        {
            ViewData["Accion"] = "Crear un nuevo producto";
            ViewBag.Valor1 = "Este es el valor 1";
            ViewBag.Nombre = "Este es el nombre";
            ViewBag.Usuario = "Usuario Que inicio sesión";

            var producto = new Producto
                {
                    Categorias = CategoriaBL.Instancia.Listar()
                };
            return View("Create", producto);
        }


        [HttpPost]
        public ActionResult Create(Producto producto)
        {
            if (ModelState.IsValid)
            {
                var fileName = Path.GetFileName(producto.File.FileName);
                if (Path.GetExtension(fileName).CompareTo(".jpg") == 0)
                {
                    var fileNamePath = Server.MapPath(@"~\Archivos\" + fileName);
                    producto.File.SaveAs(fileNamePath);
                    producto.Imagen = string.Format("/Archivos/{0}", fileName);
                    ProductoBL.Instancia.Agregar(producto);
                }

            }
            var lista = ProductoBL.Instancia.Listar();
            return View("Listado", lista);
        }

        public ActionResult Edit(int idProducto)
        {
            var producto = ProductoBL.Instancia.Obtener(idProducto);
            producto.Categorias = CategoriaBL.Instancia.Listar();
            return View("Edit", producto);

        }

        [HttpPost]
        public ActionResult Edit(Producto producto)
        {
            var fileName = Path.GetFileName(producto.File.FileName);
            var fileNamePath = Server.MapPath(@"\Archivos\" + fileName);
            var fileNameArchivoNuevo = string.Format("/Archivos/{0}", fileName);

            if (producto.Imagen.CompareTo(fileNameArchivoNuevo) != 0)
            {
                var archivoEliminar = Server.MapPath(@"~" + producto.Imagen.Replace("/", "\\"));
                    if (System.IO.File.Exists(archivoEliminar))
                    {
                        System.IO.File.Delete(archivoEliminar);
                    }
            }

            producto.File.SaveAs(fileNamePath);

            producto.Imagen = fileNameArchivoNuevo;
           
            ProductoBL.Instancia.Modificar(producto);

            var lista = ProductoBL.Instancia.Listar();
            return View("Listado", lista);
        }

        public ActionResult Delete(int idProducto)
        {
            var producto = ProductoBL.Instancia.Obtener(idProducto);

            return View("Delete", producto);
        }

        [HttpPost]
        public ActionResult Delete(Producto producto)
        {
            var entidad = ProductoBL.Instancia.Obtener(producto.IdProducto);

            ProductoBL.Instancia.Eliminar(entidad.IdProducto);

            var archivoEliminar = Server.MapPath(@"~" + entidad.Imagen.Replace("/", "\\"));
            if(System.IO.File.Exists(archivoEliminar))
            {
                System.IO.File.Delete(archivoEliminar);
            }

            var lista = ProductoBL.Instancia.Listar();
            return View("Listado", lista);
        }

        public ActionResult Details(int idProducto)
        {
            var producto = ProductoBL.Instancia.Obtener(idProducto);

            return View("Details", producto);
        }
    }
}
