﻿

using System;
using System.Collections.Generic;
using System.Linq;

namespace SistemaRestaurante.Utils
{
    public class Utils
    {
        public static List<Comun> EnumToList<T>()
        {
            var enumType = typeof(T);

            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("T debe ser de tipo System.Enum");

            var enumValArray = Enum.GetValues(enumType);
            var enumValList = (from object l in enumValArray
                               select new Comun
                               {
                                   Id = Convert.ToInt32(l),
                                   Nombre = Enum.GetName(enumType, l)
                               })
                .OrderBy(p => p.Nombre)
                .ToList();
            return enumValList;
        }

        public static List<Comun> ConvertToComunList<T>(IList<T> list, string value, string text)
        {
            var listItems = (from entity in list
                             let propiedad1 = entity.GetType().GetProperty(value)
                             where propiedad1 != null
                             let valor1 = propiedad1.GetValue(entity, null)
                             where valor1 != null
                             let propiedad2 = entity.GetType().GetProperty(text)
                             where propiedad2 != null
                             let valor2 = propiedad2.GetValue(entity, null)
                             where valor2 != null
                             select new Comun
                             {
                                 Id = Convert.ToInt32(valor1),
                                 Nombre = valor2.ToString()
                             })
                .OrderBy(p => p.Nombre)
                .ToList();
            listItems.Insert(0, new Comun { Nombre = "-- Seleccionar --", Id = 0 });
            return listItems;
        }

    }
}
