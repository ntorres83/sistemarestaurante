﻿using System;

namespace SistemaRestaurante.Utils
{
    [Serializable]
    public class JQgrid
    {
        public int total;
        public int page;
        public int records;
        public int start;
        public Row[] rows;
    }


    [Serializable]
    public class Row
    {
        public int id;
        public string[] cell;
    }
}